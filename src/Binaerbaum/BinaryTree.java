/**
 * 
 */
package Binaerbaum;

/**
 * 21.10.2016 / 16:11:44, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 16:11:44
 *
 */
public interface BinaryTree {

	/**
	 * 21.10.2016 / 16:12:25, Christian Gibt den im Wurzelknoten gehaltenen
	 * int-Wert zurueck.
	 * 
	 * @return
	 */
	int getValue();

	/**
	 * 21.10.2016 / 16:13:21, Christian Gibt den gesamten linken Teilbaum
	 * zurueck.
	 * 
	 * @ensure return != null
	 * @return
	 */
	BinaryTree getLeftTree();

	/**
	 * 21.10.2016 / 16:13:21, Christian Gibt den gesamten rechten Teilbaum
	 * zurueck.
	 * 
	 * @ensure return no!= null
	 * @return
	 */
	BinaryTree getRightTree();

	/**
	 * 21.10.2016 / 17:01:34, Christian Liefert die Summe aller Int-Werte der
	 * Knoten zurueck.
	 * 
	 * @return
	 */
	int getSum();

	/**
	 * 21.10.2016 / 17:02:18, Christian Liefert die Anzahl der im Baum
	 * enthaltenen Knoten zurueck.
	 * 
	 * @return
	 */
	int getSize();
}
