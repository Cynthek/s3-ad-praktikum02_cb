/**
 * 
 */
package Binaerbaum;

/**
 * 21.10.2016 / 16:35:41, Christian
 *
 * @author Christian Bargmann
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 16:35:41
 *
 */
public class LinkedBinaryTree implements BinaryTree {

	private Node _root;

	/**
	 * 21.10.2016 / 16:36:44, Christian Konstruktor fuer einen neuen
	 * LinkedBinaryTree
	 */
	public LinkedBinaryTree(int value) {
		// Was soll das?!
	}

	/**
	 * 21.10.2016 / 16:36:44, Christian Konstruktor fuer einen neuen
	 * LinkedBinaryTree
	 */
	public LinkedBinaryTree(int value, LinkedBinaryTree leftTree) {
		_root = new Node(value, leftTree.gibRoot());
	}

	/**
	 * 21.10.2016 / 16:36:44, Christian Konstruktor fuer einen neuen
	 * LinkedBinaryTree
	 */
	public LinkedBinaryTree(int value, LinkedBinaryTree leftTree, LinkedBinaryTree rightTree) {
		_root = new Node(value, leftTree.gibRoot(), rightTree.gibRoot());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Binaerbaum.BinaryTree#getValue()
	 */
	@Override
	public int getValue() {
		return _root.getValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Binaerbaum.BinaryTree#getLeftTree()
	 */
	@Override
	public LinkedBinaryTree getLeftTree() {
		return new LinkedBinaryTree(this.getValue(), this.getLeftTree());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Binaerbaum.BinaryTree#getRightTree()
	 */
	@Override
	public LinkedBinaryTree getRightTree() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Binaerbaum.BinaryTree#getSum()
	 */
	public int getSum() {
		return _root.getSum();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Binaerbaum.BinaryTree#getSize()
	 */
	public int getSize() {
		return _root.getNodeCount();
	}

	/**
	 * 21.10.2016 / 17:18:03, Christian Gibt den Baum auf der Konsole aus
	 */
	public void zeichneBaum() {
		if (_root == null) {
			System.out.println("");
		} else {
			System.out.println(_root.printNodeValue());
		}
	}

	/**
	 * 21.10.2016 / 16:50:02, Christian Liefert den Wurzelknoten zurueck.
	 * 
	 * @return
	 */
	private Node gibRoot() {
		return _root;
	}

}
