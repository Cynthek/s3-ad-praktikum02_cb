/**
 * 
 */
package Binaerbaum;

/**
 * 21.10.2016 / 16:16:38, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 * 
 * @since 21.10.2016 , 16:16:38
 *
 */
public class Node {

	private Node _leftChild;
	private Node _rightChild;
	private int _value;

	/**
	 * 21.10.2016 / 16:29:20, Christian Konstruktor fuer einen neuen Node
	 * 
	 * @param value
	 */
	public Node(int value) {
		_value = value;
	}

	/**
	 * 21.10.2016 / 16:29:20, Christian Konstruktor fuer einen neuen Node
	 * 
	 * @param value
	 */
	public Node(int value, Node leftChild) {
		_value = value;
		_leftChild = leftChild;
	}

	/**
	 * 21.10.2016 / 16:29:20, Christian Konstruktor fuer einen neuen Node
	 * 
	 * @param value
	 */
	public Node(int value, Node leftChild, Node rightChild) {
		_value = value;
		_leftChild = leftChild;
		_rightChild = rightChild;
	}

	/**
	 * 21.10.2016 / 16:30:12, Christian Liefert den linken Knoten zurueck.
	 * 
	 * @return Node
	 */
	Node getLeftChild() {
		return _leftChild;
	}

	/**
	 * 21.10.2016 / 16:30:36, Christian Liefert den rechten Knoten zurueck.
	 * 
	 * @return Node
	 */
	Node getRightChild() {
		return _rightChild;
	}

	/**
	 * 21.10.2016 / 16:30:58, Christian Liefert den im Knoten gespeicherten Wert
	 * zurueck.
	 * 
	 * @return value
	 */
	int getValue() {
		return _value;
	}

	/**
	 * 21.10.2016 / 16:58:56, Christian Liefert die Summe der Int-Werte aller
	 * Knoten.
	 * 
	 * @return
	 */
	public int getSum() {
		int summe = this.getValue();
		if (_leftChild != null)
			summe = summe + _leftChild.getValue();
		if (_rightChild != null)
			summe = summe + _rightChild.getValue();
		return summe;
	}

	/**
	 * 21.10.2016 / 17:03:55, Christian Liefert die Anzahl aller im Baum
	 * enthaltenen Knoten zurueck.
	 * 
	 * @return
	 */
	public int getNodeCount() {
		int result = 1;
		if (_leftChild != null) {
			result = result + _leftChild.getNodeCount();
		}
		if (_rightChild != null) {
			result = result + _rightChild.getNodeCount();
		}
		return result;
	}

	/**
	 * 21.10.2016 / 17:04:51, Christian Liefert eine StringRepraesentation aller
	 * Int-Werte im Knoten zurueck.
	 * 
	 * @return
	 */
	public String printNodeValue() {
		String result = "" + getValue();
		if (_leftChild != null) {
			result = result + "," + _leftChild.printNodeValue() + "\n";
		}
		if (_rightChild != null) {
			result = result + _rightChild.printNodeValue() + "\n";
		}
		return result;
	}

}
