package Binaerbaum.startup;

import Binaerbaum.LinkedBinaryTree;

/**
 * 21.10.2016 / 16:03:01, Christian
 *
 * @author Christian
 * @author (co-author)
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 16:03:01
 *
 */
public class Main {

	public static void main(String args[]) {
		LinkedBinaryTree ast1 = new LinkedBinaryTree(1);
		LinkedBinaryTree ast2 = new LinkedBinaryTree(5);
		LinkedBinaryTree baum1 = new LinkedBinaryTree(1, ast1, ast2);

		LinkedBinaryTree ast3 = new LinkedBinaryTree(13);
		LinkedBinaryTree ast4 = new LinkedBinaryTree(2);
		LinkedBinaryTree baum2 = new LinkedBinaryTree(7, ast3, ast4);
		LinkedBinaryTree gesamtBaum = new LinkedBinaryTree(50, baum1, baum2);

		System.out.println(gesamtBaum.getSum());
		System.out.println(gesamtBaum.getSize());
		gesamtBaum.zeichneBaum();
	}

}
