package Widerstandsnetz;
/**
 * 
 */

/**
 * 21.10.2016 / 14:18:53, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 14:18:53
 *
 */
public abstract class AbstractWiderstandsnetz implements Widerstandsnetz {

	protected double _gesamtWiderstand;
	protected int _anzahlWiderstaende;

	@Override
	public double getOhm() {
		assert _gesamtWiderstand >= 0 : "Gesamtwiderstand ist kleiner als 0";
		return _gesamtWiderstand;
	}

	@Override
	public int getAnzahlWiderstaende() {
		return _anzahlWiderstaende;
	}

}
