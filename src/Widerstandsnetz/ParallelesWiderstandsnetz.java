package Widerstandsnetz;
/**
 * 21.10.2016 / 14:29:44, Christian Diese Klasse representiert ein
 * Widerstandsnetz im Parallelschaltung. Berechnung der Widerstaende erfolgt
 * durch die Formel: 1/R = 1/R1 + 1/R2 also gilt: R = 1 / (1/R1 + 1/R2)
 * 
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 14:29:44
 *
 */
public class ParallelesWiderstandsnetz extends ZusammengesetztesWiderstandsnetz {

	/**
	 * 21.10.2016 / 14:43:21, Christian
	 *
	 * @param widerstandsnetz1
	 * @param widerstandsnetz2
	 */
	public ParallelesWiderstandsnetz(AbstractWiderstandsnetz widerstandsnetz1,
			AbstractWiderstandsnetz widerstandsnetz2) {
		super(widerstandsnetz1, widerstandsnetz2);
		_gesamtWiderstand = 1 / (1 / widerstandsnetz1.getOhm() + 1 / widerstandsnetz2.getOhm());
		_anzahlWiderstaende = widerstandsnetz1.getAnzahlWiderstaende() + widerstandsnetz2.getAnzahlWiderstaende();
	}

}
