package Widerstandsnetz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 21.10.2016 / 15:24:09, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 15:24:09
 *
 */
public class ParallelesWiderstandsnetzTest {

	@Test
	public void getOhm() {
		ParallelesWiderstandsnetz parallel = new ParallelesWiderstandsnetz(new Widerstand(400), new Widerstand(200));
		assertEquals(parallel.getOhm(), 133.33333333333333, 0.1);

	}

	@Test
	public void zusammengesetztesNetz01() {
		ParallelesWiderstandsnetz parallel = new ParallelesWiderstandsnetz(new Widerstand(400), new Widerstand(135));
		assertEquals(parallel.getAnzahlWiderstaende(), 2);
	}

	@Test
	public void zusammengesetztesNetz02() {
		ParallelesWiderstandsnetz netz1 = new ParallelesWiderstandsnetz(new Widerstand(400), new Widerstand(135));
		ParallelesWiderstandsnetz netz2 = new ParallelesWiderstandsnetz(new Widerstand(345), new Widerstand(746));
		ParallelesWiderstandsnetz zusammengesetzt = new ParallelesWiderstandsnetz(netz1, netz2);
		assertEquals(zusammengesetzt.getAnzahlWiderstaende(), 4);
	}

	@Test
	public void zusammengesetztesNetz03() {
		ParallelesWiderstandsnetz netz1 = new ParallelesWiderstandsnetz(new Widerstand(400), new Widerstand(135));
		ParallelesWiderstandsnetz netz2 = new ParallelesWiderstandsnetz(new Widerstand(345), new Widerstand(746));
		ParallelesWiderstandsnetz zusammengesetzt = new ParallelesWiderstandsnetz(netz1, netz2);
		ParallelesWiderstandsnetz netz3 = new ParallelesWiderstandsnetz(new Widerstand(123), new Widerstand(15));
		ParallelesWiderstandsnetz netz4 = new ParallelesWiderstandsnetz(new Widerstand(98), new Widerstand(43));
		ParallelesWiderstandsnetz zusammengesetzt2 = new ParallelesWiderstandsnetz(netz3, netz4);
		ParallelesWiderstandsnetz testnetz = new ParallelesWiderstandsnetz(zusammengesetzt, zusammengesetzt2);
		assertEquals(testnetz.getAnzahlWiderstaende(), 8);
	}

}
