/**
 * 
 */
package Widerstandsnetz;

/**
 * 21.10.2016 / 15:11:40, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 15:11:40
 *
 */
public class Potenziometer extends Widerstand {

	/**
	 * 21.10.2016 / 15:11:47, Christian
	 *
	 * @param wiederstandswert
	 */
	public Potenziometer(double wiederstandswert) {
		super(wiederstandswert);
	}

	public void setOhm(double widerstand) {
		_gesamtWiderstand = widerstand;
	}

}
