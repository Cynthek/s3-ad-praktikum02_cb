package Widerstandsnetz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 21.10.2016 / 15:43:33, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 15:43:33
 *
 */
public class PotenziometerTest {

	@Test
	public void testSetOhm() {
		Potenziometer potenziometer = new Potenziometer(400);
		potenziometer.setOhm(200);
		assertEquals(potenziometer.getOhm(), 200, 0);
		potenziometer.setOhm(11);
		assertEquals(potenziometer.getOhm(), 11, 0);

	}

	@Test
	public void testGetAnzahlWiderstaende() {
		Potenziometer widerstand = new Potenziometer(500);
		assertEquals(widerstand.getAnzahlWiderstaende(), 1);
		Potenziometer widerstand2 = new Potenziometer(435);
		assertEquals(widerstand2.getAnzahlWiderstaende(), 1);
	}

	@Test
	public void testGetOhm() {
		Potenziometer potenzi = new Potenziometer(400);
		assertEquals(potenzi.getOhm(), 400, 0);
		Potenziometer tenzi = new Potenziometer(3350);
		assertEquals(tenzi.getOhm(), 3350, 0);

	}
}
