package Widerstandsnetz;

/**
 * 21.10.2016 / 14:29:04, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 14:29:04
 *
 */
public class SeriellesWiderstandsnetz extends ZusammengesetztesWiderstandsnetz {

	/**
	 * 21.10.2016 / 14:50:19, Christian
	 *
	 * @param widerstandsnetz1
	 * @param widerstandsnetz2
	 */
	public SeriellesWiderstandsnetz(AbstractWiderstandsnetz widerstandsnetz1,
			AbstractWiderstandsnetz widerstandsnetz2) {
		super(widerstandsnetz1, widerstandsnetz2);
		_gesamtWiderstand = widerstandsnetz1.getOhm() + widerstandsnetz2.getOhm();
		_anzahlWiderstaende = widerstandsnetz1.getAnzahlWiderstaende() + widerstandsnetz2.getAnzahlWiderstaende();
	}

}
