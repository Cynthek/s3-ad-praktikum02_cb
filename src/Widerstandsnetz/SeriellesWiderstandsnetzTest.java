/**
 * 
 */
package Widerstandsnetz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 21.10.2016 / 15:45:22, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 * @since 21.10.2016 , 15:45:22
 *
 */
public class SeriellesWiderstandsnetzTest {

	@Test
	public void testZusammengesetztesNetz01() {
		SeriellesWiderstandsnetz gesamtNetz = new SeriellesWiderstandsnetz(new Widerstand(400), new Widerstand(135));
		assertEquals(gesamtNetz.getAnzahlWiderstaende(), 2);
	}

	@Test
	public void testZusammengesetztesNetz02() {
		SeriellesWiderstandsnetz seriell01 = new SeriellesWiderstandsnetz(new Widerstand(400), new Widerstand(135));
		SeriellesWiderstandsnetz seriell02 = new SeriellesWiderstandsnetz(new Widerstand(345), new Widerstand(746));
		SeriellesWiderstandsnetz gesamtNetz = new SeriellesWiderstandsnetz(seriell01, seriell02);
		assertEquals(gesamtNetz.getAnzahlWiderstaende(), 4);
	}

	@Test
	public void testZusammengesetztesNetz03() {
		SeriellesWiderstandsnetz seriell01 = new SeriellesWiderstandsnetz(new Widerstand(400), new Widerstand(135));
		SeriellesWiderstandsnetz seriell02 = new SeriellesWiderstandsnetz(new Widerstand(345), new Widerstand(746));
		SeriellesWiderstandsnetz gesamtNetz = new SeriellesWiderstandsnetz(seriell01, seriell02);
		SeriellesWiderstandsnetz seriell03 = new SeriellesWiderstandsnetz(new Widerstand(123), new Widerstand(15));
		SeriellesWiderstandsnetz seriell04 = new SeriellesWiderstandsnetz(new Widerstand(98), new Widerstand(43));
		SeriellesWiderstandsnetz gesamtNetz2 = new SeriellesWiderstandsnetz(seriell03, seriell04);
		SeriellesWiderstandsnetz gesamtNetz3 = new SeriellesWiderstandsnetz(gesamtNetz, gesamtNetz2);
		assertEquals(gesamtNetz3.getAnzahlWiderstaende(), 8);
	}
}
