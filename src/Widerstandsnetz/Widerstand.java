package Widerstandsnetz;
/**
 * 
 */

/**
 * 21.10.2016 / 14:21:18, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 14:21:18
 *
 */
public class Widerstand extends AbstractWiderstandsnetz {

	/**
	 * 21.10.2016 / 14:24:51, Christian Konstruktor fuer neue Widertaende
	 */
	public Widerstand(double wiederstandswert) {
		_gesamtWiderstand = wiederstandswert;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see AbstractWiderstandsnetz#getAnzahlWiderstaende()
	 */
	@Override
	public int getAnzahlWiderstaende() {
		return 1;
	}
}
