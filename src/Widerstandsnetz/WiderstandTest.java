/**
 * 
 */
package Widerstandsnetz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 21.10.2016 / 15:52:53, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 15:52:53
 *
 */
public class WiderstandTest {

	@Test
	public void testGetOhm() {
		Widerstand widerstand = new Widerstand(500);
		assertEquals(widerstand.getOhm(), 500, 0);
		Widerstand widerstand2 = new Widerstand(0);
		assertEquals(widerstand2.getOhm(), 0, 0);
	}

	@Test
	public void testGetAnzahlWiderstaende() {
		Widerstand widerstand = new Widerstand(500);
		assertEquals(widerstand.getAnzahlWiderstaende(), 1);
		Widerstand widerstand2 = new Widerstand(435);
		assertEquals(widerstand2.getAnzahlWiderstaende(), 1);
	}
}
