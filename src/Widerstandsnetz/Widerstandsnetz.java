package Widerstandsnetz;
/**
 * 21.10.2016 / 14:15:38, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 14:15:38
 *
 */
public interface Widerstandsnetz {

	/**
	 * 21.10.2016 / 14:16:16, Christian Liefert den Gesamtwiderstand des Netzes,
	 * dieser ist immer mindestens 0
	 * 
	 * @ensure gesamtwiderstand >= 0
	 * @return
	 */
	abstract double getOhm();

	/**
	 * 21.10.2016 / 14:16:33, Christian Liefert die Anzahl an einfachen
	 * Widerstaenden im Netz
	 * 
	 * @return
	 */
	abstract int getAnzahlWiderstaende();

}
