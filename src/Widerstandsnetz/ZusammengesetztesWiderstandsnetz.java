package Widerstandsnetz;
/**
 * 21.10.2016 / 14:26:40, Christian
 *
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 14:26:40
 *
 */
public abstract class ZusammengesetztesWiderstandsnetz extends AbstractWiderstandsnetz {

	protected AbstractWiderstandsnetz _widerstandsnetz1;
	protected AbstractWiderstandsnetz _widerstandsnetz2;

	/**
	 * Initialisiert ein Widerstandsnetz aus zwei Widerstandsnetzen.
	 * 
	 * @param widerstandsnetz1
	 * @param widerstandsnetz2
	 * @require widerstandsnetz1!=null && widerstandsnetz2!=null
	 */
	public ZusammengesetztesWiderstandsnetz(AbstractWiderstandsnetz widerstandsnetz1,
			AbstractWiderstandsnetz widerstandsnetz2) {
		assert widerstandsnetz1 != null : "Vorbedingung verletzt: erstesNetz != null";
		assert widerstandsnetz2 != null : "Vorbedingung verletzt: zweitesNetz != null";
		_widerstandsnetz1 = widerstandsnetz1;
		_widerstandsnetz2 = widerstandsnetz2;

	}

}
