package Widerstandsnetz.startup;

import Widerstandsnetz.ParallelesWiderstandsnetz;
import Widerstandsnetz.Potenziometer;
import Widerstandsnetz.SeriellesWiderstandsnetz;
import Widerstandsnetz.Widerstand;

/**
 * 
 * 21.10.2016 / 16:00:31, Christian Testprogramm zum Umsetzen des
 * Widerstandsnetzes auf dem Aufgabenzettel mit dem Potentiometer
 * 
 * @author Christian
 * @author Julia-Marie Stache
 * @version 21.10.2016
 *
 * @since 21.10.2016 , 16:01:09
 *
 */
public class Main2 {

	/**
	 * 
	 * 20.10.2016 / 11:04:26, Christian
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		// Initialisieren der einzelnen Widertaende
		Widerstand widerstandsKnoten[] = new Widerstand[6];

		for (int i = 0; i < 6; i++) {
			widerstandsKnoten[i] = new Widerstand(100 * (i + 1));
		}

		for (int i = 0; i <= 5000; i = i + 200) {
			widerstandsKnoten[3] = new Potenziometer(i);
			// Zusammengesetze Widerstandsnetze starten
			ParallelesWiderstandsnetz parallelNetz01;
			ParallelesWiderstandsnetz parallelNetz02;
			ParallelesWiderstandsnetz parallelNetz03;
			SeriellesWiderstandsnetz seriellesNetz01;
			SeriellesWiderstandsnetz seriellesNetz02;
			// Obere Reihe
			parallelNetz01 = new ParallelesWiderstandsnetz(widerstandsKnoten[0], widerstandsKnoten[2]);
			seriellesNetz01 = new SeriellesWiderstandsnetz(parallelNetz01, widerstandsKnoten[1]);
			// Mittlere Reihe
			seriellesNetz02 = new SeriellesWiderstandsnetz(widerstandsKnoten[3], widerstandsKnoten[4]);
			parallelNetz02 = new ParallelesWiderstandsnetz(seriellesNetz02, seriellesNetz01);
			// Untere Reihe
			parallelNetz03 = new ParallelesWiderstandsnetz(parallelNetz02, widerstandsKnoten[5]);
			// Ausgabe
			System.out.println("Widerstand des Potentiometers ist " + widerstandsKnoten[3].getOhm());
			System.out.println("Gesamtwiderstand ist " + parallelNetz03.getOhm());
			System.out.println("Anzahl Widerstaende ist  " + parallelNetz03.getAnzahlWiderstaende());
			System.out.println("\n");
		}

	}

}
